<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="de.tum.in.dbpra.model.bean.OrderBean" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Orders</title>
</head>
<body>
<c:choose>
	<c:when test="${not empty error}">
		<h2>Error occurred: ${error}</h2>
	</c:when>
	<c:otherwise>
		<h1> Orders with status "ok"</h1>
		<c:choose>
			<c:when test="${not empty ordersok and ordersok.size() > 0}">
				<table>
					<tr>
					  <th>Order Key</th>
					  <th>Customer Key</th> 
					  <th>Order Status</th>
					  <th>Total price</th> 
					  <th>Order date</th>
					</tr>	
					<c:forEach items="${ordersok}" var="order">
						<tr>
							<td>${order.orderKey}</td>
							<td>${order.custKey}</td>
							<td>${order.orderStatus}</td>
							<td>${order.totalPrice}</td>
							<td>${order.orderDate}</td>
						</tr>
					</c:forEach>
				</table>
			</c:when>
			<c:when test="${not empty errorok}">
				${errorok}
			</c:when>
			<c:otherwise>
				<i>No orders with status "ok" found in the database.</i>
			</c:otherwise> 
		</c:choose>
		<br>
		<h1> Orders with status "no"</h1>
		<c:choose>
			<c:when test="${not empty ordersno and ordersno.size() > 0}">   
				 <table>
					<tr>
					  <th>Order Key</th>
					  <th>Customer Key</th> 
					  <th>Order Status</th>
					  <th>Total price</th> 
					  <th>Order date</th>
					</tr>
					<c:forEach items="${ordersno}" var="order">
						
						<tr>
							<td>${order.orderKey}</td>
							<td>${order.custKey}</td>
							<td>${order.orderStatus}</td>
							<td>${order.totalPrice}</td>
							<td>${order.orderDate}</td>
						</tr>
					</c:forEach>
				</table>
			</c:when>
			<c:when test="${not empty errorno}">
				${errorno}
			</c:when>
			<c:otherwise>
				<i>No orders with status "no" found in the database.</i>
			</c:otherwise> 
		</c:choose>	
	</c:otherwise>
</c:choose>
<p>
<a href="/Exercise7-Group7/exercise71.html">Back to the list of exercises</a>        		
</body>
</html>