<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="de.tum.in.dbpra.model.bean.PartBean" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Parts</title>
</head>
<body>
<p><h1> Parts: </h1> </p>
<c:choose>
	<c:when test="${error==null}">
		<table>
			<tr>
			  <th><a href="/Exercise7-Group7/exercise73?sortby=partkey&keyword=${keyword}&searchby=${searchby}&searchMode=${searchMode}">Part Key</a></th>
			  <th><a href="/Exercise7-Group7/exercise73?sortby=name&keyword=${keyword}&searchby=${searchby}&searchMode=${searchMode}">Name</a></th> 
			  <th><a href="/Exercise7-Group7/exercise73?sortby=type&keyword=${keyword}&searchby=${searchby}&searchMode=${searchMode}">Type</a></th>
			  <th><a href="/Exercise7-Group7/exercise73?sortby=size&keyword=${keyword}&searchby=${searchby}&searchMode=${searchMode}">Size</a></th> 
			  <th><a href="/Exercise7-Group7/exercise73?sortby=container&keyword=${keyword}&searchby=${searchby}&searchMode=${searchMode}">Container</a></th>
			  <th><a href="/Exercise7-Group7/exercise73?sortby=retailprice&keyword=${keyword}&searchby=${searchby}&searchMode=${searchMode}">Retail Price</a></th>
			</tr>	
			<c:forEach items="${parts}" var="part">
			
				<tr>
					<td>
						<c:choose>
						<c:when test="${part.highlighted}"><b>${part.partKey}</b></c:when>
						<c:otherwise>${part.partKey}</c:otherwise> 
						</c:choose>
					<td>
						<c:choose>
						<c:when test="${part.highlighted}"><b>${part.name}</b></c:when>
						<c:otherwise>${part.name}</c:otherwise> 
						</c:choose>
					</td>
					<td>
						<c:choose>
						<c:when test="${part.highlighted}"><b>${part.type}</b></c:when>
						<c:otherwise>${part.type}</c:otherwise> 
						</c:choose>
					</td>
					<td><c:choose>
						<c:when test="${part.highlighted}"><b>${part.size}</b></c:when>
						<c:otherwise>${part.size}</c:otherwise> 
						</c:choose>
					</td>
					<td><c:choose>
						<c:when test="${part.highlighted}"><b>${part.container}</b></c:when>
						<c:otherwise>${part.container}</c:otherwise> 
						</c:choose>
					</td>
					<td><c:choose>
						<c:when test="${part.highlighted}"><b>${part.retailPrice}</b></c:when>
						<c:otherwise>${part.retailPrice}</c:otherwise> 
						</c:choose>
					</td>
				</tr>		
			</c:forEach>
		</table>
	</c:when>
	<c:otherwise>${error}</c:otherwise> 
</c:choose>

<h1>Search:</h1>
<form action="/Exercise7-Group7/exercise73" method="get">
	Keyword: <input type="text" name="keyword" value="${keyword}" /><br />
	Search by column: <select name="searchby">			
		<option value="partkey"  ${"partkey" == searchby ? 'selected="selected"' : ''}>Part Key</option>
		<option value="name" ${"name" == searchby ? 'selected="selected"' : ''}>Name</option>
		<option value="type" ${"type" == searchby ? 'selected="selected"' : ''}>Type</option>
		<option value="size" ${"size" == searchby ? 'selected="selected"' : ''}>Size</option>
		 <option value="container" ${"container" == searchby ? 'selected="selected"' : ''}>Container</option>
		<option value="retailprice" ${"retailprice" == searchby ? 'selected="selected"' : ''}>Retail Price</option>
	</select>
		<br>
	<input type="radio" name="searchMode" value="exact" checked="checked">Exact<br>
	<input type="radio" name="searchMode" value="like" ${"like" == searchMode ? 'checked="checked"' : ''}>Like<br>
	<input type="submit" name="submit" value="Search" />		  
</form>
<br>
<a href="/Exercise7-Group7/exercise71.html">Back to the list of exercises</a>
</body>
</html>