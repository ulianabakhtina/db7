<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="de.tum.in.dbpra.model.bean.OrderBean" %>
<%@page import="de.tum.in.dbpra.model.bean.LineitemBean" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Order Details</title>
</head>

<c:choose>
	<c:when test="${empty errorOrder and empty error}">
		<h2>INVOICE</h2>
		Customer Name: <c:out value="${order.customer.name} "/><br />
		Customer Number: <c:out value="${order.customer.custkey} "/><br />
		Invoice Form Number (Order Key): <c:out value="${order.orderKey} "/><br />
		Order Date: <c:out value="${order.orderDate} "/>
		<p>
		The order contains the following items:
		<p>
		<table border="1">
			<tr>
				<th>Linenumber</th>
				<th>Partname</th>
				<th>Quantity</th>
				<th>Price</th>
			</tr>
			<c:choose>
				<c:when test="${not empty order.lineitems and order.lineitems.size() > 0}">
					<c:forEach items="${order.lineitems}" var="lineitem">
						<tr>
							<td>${lineitem.lineNumber}</td>
							<td>${lineitem.part.name}</td>
							<td>${lineitem.quantity}</td>
							<td>${lineitem.price}</td>
						</tr>
					</c:forEach>				
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="4"><i>No lineitems exist for the selected order.</i></td>
					</tr>
				</c:otherwise>
			</c:choose>
		</table>
		<p>
		Lump Sum Price (Total Price): <c:out value="${order.totalPrice} "/><p>
</c:when>
<c:otherwise>
	${errorOrder}
	<p>
	${error}
</c:otherwise> 
</c:choose>
<br>
<a href="/Exercise7-Group7/exercise71.html">Back to the list of exercises</a>

<body>

</body>
</html>