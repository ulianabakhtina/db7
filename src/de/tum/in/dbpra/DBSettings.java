package de.tum.in.dbpra;

public class DBSettings {
	
	public static final String CONNECTION_STRING = "jdbc:postgresql://localhost:5432/PRAKT";
	public static final String DB_USERNAME = "postgres";
	public static final String DB_PASSWORD = "postgres";

}
