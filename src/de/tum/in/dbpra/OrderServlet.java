/**
 * 
 */
package de.tum.in.dbpra;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tum.in.dbpra.model.bean.OrderBean;
import de.tum.in.dbpra.model.dao.OrderDAO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

@SuppressWarnings("serial")
public class OrderServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		try {
        	OrderDAO dao = new OrderDAO();
        	Map<String, ArrayList<OrderBean>> allOrders = dao.getOrders();
        	ArrayList<OrderBean> ordersok = allOrders.get("ok");
        	ArrayList<OrderBean> ordersno = allOrders.get("no");
        	
        	if(ordersok != null && ordersok.size() > 0) {
        		request.setAttribute("ordersok", ordersok);
        	}
        	else {
        		request.setAttribute("errorok", "No orders found with orderstatus = ok");
        	}
        	
        	if(ordersno != null && ordersno.size() > 0) {
        		request.setAttribute("ordersno", ordersno);
        	}
        	else {
        		request.setAttribute("errorno", "No orders found with orderstatus = no");
        	}
        	
    	} catch (Throwable e) {
    		request.setAttribute("error", e.toString());
    		e.printStackTrace();
    	}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/exercise72.jsp");
		dispatcher.forward(request, response);
    }
    
    
}
