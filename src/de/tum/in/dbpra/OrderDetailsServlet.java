package de.tum.in.dbpra;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tum.in.dbpra.model.bean.OrderBean;
import de.tum.in.dbpra.model.dao.CustomerDAO.CustomerNotFoundException;
import de.tum.in.dbpra.model.dao.OrderDAO;
import de.tum.in.dbpra.model.dao.OrderDAO.OrdersNotFoundException;

import java.io.IOException;

@SuppressWarnings("serial")
public class OrderDetailsServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			if (request.getParameter("orderKey") != null) {
				int orderKey = Integer.parseInt(request.getParameter("orderKey"));
				OrderDAO dao = new OrderDAO();

				// TODO: Extract additional parameters from order
				// to lineitem, part and customer beans if there
				// is time for this.

				OrderBean order = dao.getOrder(orderKey);

				request.setAttribute("order", order);
			} else {
				request.setAttribute("errorOrder", "Error: No order key was specified.");
			}
		} catch (NumberFormatException e) {
			request.setAttribute("error", "Specified order key is invalid.");
		} catch (OrdersNotFoundException e) {
			request.setAttribute("errorOrder", e.getMessage());
		} catch (CustomerNotFoundException e) {
			request.setAttribute("errorCustomer", e.getMessage());
		} catch (Exception e) {
			request.setAttribute("error", e.getMessage());
		}

		RequestDispatcher dispatcher = request
				.getRequestDispatcher("/exercise74.jsp");
		dispatcher.forward(request, response);
	}

}
