package de.tum.in.dbpra;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.print.URIException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.tum.in.dbpra.model.bean.PartBean;
import de.tum.in.dbpra.model.dao.PartDAO;

@SuppressWarnings("serial")
public class PartServlet extends HttpServlet  {

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		Map<String,String> map = mapColumns();   			
		
		String keyword = request.getParameter("keyword");			
		String dbColumnSearchBy = map.get(request.getParameter("searchby"));
		String searchMode = request.getParameter("searchMode");
		String dbColumnSortBy = map.get(request.getParameter("sortby"));
		
		request.setAttribute("keyword", keyword);
    	request.setAttribute("searchby", dbColumnSearchBy);
    	request.setAttribute("searchMode", searchMode);
		
		try {
        	PartDAO dao = new PartDAO();
        	ArrayList<PartBean> parts;
						
    		parts = dao.searchParts(keyword, dbColumnSearchBy, "exact".equalsIgnoreCase(searchMode), dbColumnSortBy);

        	request.setAttribute("parts", parts);        	
        	
    	} catch (Throwable e) {
    		request.setAttribute("error", e.getMessage());
    	}
				
		RequestDispatcher dispatcher = request.getRequestDispatcher("/exercise73.jsp");
		dispatcher.forward(request, response);
    }
	
	protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
    }
	
	private Map<String,String> mapColumns() {
		Map<String,String> map = new HashMap<String,String>();
		map.put("partkey", "partkey");
		map.put("name", "name");
		map.put("type", "type");
		map.put("size", "size");
		map.put("container", "container");
		map.put("retailprice", "retailprice");
		
		return map;
	}
	
	
}
