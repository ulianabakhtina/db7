package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.tum.in.dbpra.model.bean.PartBean;

public class PartDAO extends AbstractDAO {

	public ArrayList<PartBean> getAllParts(String sortby)
			throws PartsNotFoundException, SQLException {

		String query;
		if (sortby != null && !sortby.isEmpty()) {
			query = "SELECT * FROM part order by " + sortby + " asc";
		} else {
			query = "SELECT * FROM part order by name asc";
		}
		ArrayList<PartBean> parts = new ArrayList<PartBean>();

		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_READ_ONLY);) {

			try (ResultSet resultSet = preparedStatement.executeQuery();) {

				if (resultSet.isBeforeFirst()) {

					parts = resultSetToArrayList(resultSet);
				} else {
					throw new PartsNotFoundException("Database found no parts!");
				}
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return parts;
	}

	public ArrayList<PartBean> searchParts(String keyword, String searchby,
			boolean isExact, String sortby) throws PartsNotFoundException,
			SQLException {

		String query;
		String keywordValue = "";
		boolean searchRequest = false;

		if (keyword != null && !keyword.isEmpty()) {
			searchRequest = true;
			String searchMode = "=";
			keywordValue = keyword;
			if (!isExact) {
				searchMode = "like";
				keywordValue = "%" + keyword + "%";
			}

			query = new StringBuilder().append("SELECT * ")
					.append("FROM part ").append("WHERE CAST(")
					.append(searchby).append(" AS TEXT) ").append(searchMode)
					.append(" ? ").toString();
		}

		else {
			query = "SELECT * FROM part ";
		}

		if (sortby != null && !sortby.isEmpty()) {
			query += "order by ";
			query += sortby;
			query += " asc";
		}

		else {
			query += "order by name asc";
		}

		ArrayList<PartBean> parts = new ArrayList<PartBean>();

		try (Connection connection = getConnection();
				PreparedStatement preparedStatement = connection
						.prepareStatement(query, ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_READ_ONLY);) {
			if (keyword != null && !keyword.isEmpty()) {
				preparedStatement.setString(1, keywordValue);
			}

			try (ResultSet resultSet = preparedStatement.executeQuery();) {

				if (resultSet.isBeforeFirst()) {
					parts = resultSetToArrayList(resultSet);
					if (searchRequest) {
						ArrayList<PartBean> allParts = getAllParts(sortby);

						for (int i = 0; i < allParts.size(); i++) {
							for (int j = 0; j < parts.size(); j++) {
								if (allParts.get(i).equals(parts.get(j))) {
									allParts.get(i).setHighlighted(true);
								}
							}
						}
						parts = allParts;
					}

				} else {
					throw new PartsNotFoundException(
							"Database found no parts or no parts with this keyword!");
				}
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
				throw e;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
		}
		return parts;
	}

	@SuppressWarnings("serial")
	public static class PartsNotFoundException extends Throwable {
		PartsNotFoundException(String message) {
			super(message);
		}
	}

	ArrayList<PartBean> resultSetToArrayList(ResultSet resultSet) {

		ArrayList<PartBean> parts = new ArrayList<PartBean>();
		try {
			while (resultSet.next()) {
				PartBean part = new PartBean();
				part.setPartKey(resultSet.getInt("partkey"));
				part.setName(resultSet.getString("name"));
				part.setType(resultSet.getString("type"));
				part.setSize(resultSet.getInt("size"));
				part.setContainer(resultSet.getInt("container"));
				part.setRetailPrice(resultSet.getFloat("retailprice"));
				parts.add(part);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return parts;
	}

}
