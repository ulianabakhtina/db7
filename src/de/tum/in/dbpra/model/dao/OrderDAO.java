package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.tum.in.dbpra.model.bean.CustomerBean;
import de.tum.in.dbpra.model.bean.LineitemBean;
import de.tum.in.dbpra.model.bean.OrderBean;
import de.tum.in.dbpra.model.bean.PartBean;
import de.tum.in.dbpra.model.dao.CustomerDAO.CustomerNotFoundException;

public class OrderDAO extends AbstractDAO {
	
	public Map<String, ArrayList<OrderBean>> getOrders() throws SQLException {

		String query = new StringBuilder().append("SELECT * FROM orders ")
				.append("WHERE orderstatus = ?").toString();

		ArrayList<OrderBean> ordersOk = new ArrayList<OrderBean>();
		ArrayList<OrderBean> ordersNo = new ArrayList<OrderBean>();
		Map<String, ArrayList<OrderBean>> allOrders = new HashMap<String, ArrayList<OrderBean>>();
		Connection connection = null;

		try {
			connection = getConnection();
			connection.setAutoCommit(false);

			PreparedStatement preparedStatement1 = connection.prepareStatement(
					query, ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			preparedStatement1.setString(1, "ok");
			ResultSet resultSet1 = preparedStatement1.executeQuery();

			if (resultSet1.isBeforeFirst()) {
				ordersOk = orderResultSetToArrayList(resultSet1);
				allOrders.put("ok", ordersOk);
			}

			resultSet1.close();

			PreparedStatement preparedStatement2 = connection.prepareStatement(
					query, ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			preparedStatement2.setString(1, "no");
			ResultSet resultSet2 = preparedStatement2.executeQuery();

			if (resultSet2.isBeforeFirst()) {
				ordersNo = orderResultSetToArrayList(resultSet2);
				allOrders.put("no", ordersNo);
			}

			resultSet2.close();

			connection.commit();

		} catch (SQLException e) {
			if (connection != null) {
				connection.rollback();
			}
				
			e.printStackTrace();
			throw e;
		}
		return allOrders;
	}

	public OrderBean getOrder(int orderKey) throws OrdersNotFoundException,
			SQLException, CustomerNotFoundException {

		String queryOrder = new StringBuilder().append("SELECT * FROM orders ")
				.append("WHERE orderkey = ?").toString();

		String queryLineitems = new StringBuilder()
				.append("SELECT orderkey, linenumber, quantity, extendedprice, name, p.partkey, "
						+ "type, size, container, retailprice "
						+ "FROM lineitem l LEFT OUTER JOIN part p ")
				.append("ON l.partkey = (p.partkey) WHERE ")
				.append("orderkey = ? ").append("order by linenumber")
				.toString();
		
		OrderBean order;
		CustomerDAO customerDAO = new CustomerDAO();

		Connection connection = null;

		try {
			connection = getConnection();
			connection.setAutoCommit(false);

			PreparedStatement preparedStatement1 = connection.prepareStatement(
					queryOrder, ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);

			preparedStatement1.setInt(1, orderKey);
			ResultSet resultSetOrder = preparedStatement1.executeQuery();

			if (resultSetOrder.isBeforeFirst()) {
				order = orderResultSetToArrayList(resultSetOrder).get(0);
				CustomerBean customer = new CustomerBean();
				customer.setCustkey(order.getCustKey());
				customerDAO.getCustomerByID(customer); // TODO: customer not found exception
				order.setCustomer(customer);

				PreparedStatement preparedStatement2 = connection
						.prepareStatement(queryLineitems,
								ResultSet.TYPE_FORWARD_ONLY,
								ResultSet.CONCUR_READ_ONLY);

				preparedStatement2.setInt(1, orderKey);
				ResultSet resultSetLineitems = preparedStatement2.executeQuery();

				if (resultSetLineitems.isBeforeFirst()) {
					ArrayList<LineitemBean> lineitems = lineitemResultSetToArrayList(resultSetLineitems, order);
					order.setLineitems(lineitems);
				}
				resultSetLineitems.close();
			} else {
				throw new OrdersNotFoundException("Database found no order with this orderkey!");
			}
			resultSetOrder.close();

			connection.commit();

		} catch (SQLException e) {
			connection.rollback();
			e.printStackTrace();
			throw e;
		}
		return order;

	}

	private ArrayList<OrderBean> orderResultSetToArrayList(ResultSet resultSet) {

		ArrayList<OrderBean> orders = new ArrayList<OrderBean>();
		try {
			while (resultSet.next()) {
				OrderBean order = new OrderBean();
				order.setOrderKey(resultSet.getInt("orderkey"));
				order.setOrderStatus(resultSet.getString("orderstatus"));
				order.setTotalPrice(resultSet.getFloat("totalprice"));
				order.setOrderDate(resultSet.getDate("orderdate"));
				order.setCustKey(resultSet.getInt("custkey"));
				orders.add(order);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return orders;
	}

	private ArrayList<LineitemBean> lineitemResultSetToArrayList(
			ResultSet resultSet, OrderBean order) {

		ArrayList<LineitemBean> lineitems = new ArrayList<LineitemBean>();
		try {
			while (resultSet.next()) {
				LineitemBean lineitem = new LineitemBean();
				lineitem.setOrder(order);
				lineitem.setLineNumber(resultSet.getInt("linenumber"));
				lineitem.setQuantity(resultSet.getInt("quantity"));
				lineitem.setPrice(resultSet.getFloat("extendedprice"));
				
				if (resultSet.getString("partkey") != null) {
					PartBean part = new PartBean();
					part.setLineitem(lineitem);
					part.setPartKey(resultSet.getInt("partkey"));
					part.setName(resultSet.getString("name"));
					part.setType(resultSet.getString("type"));
					part.setSize(resultSet.getInt("size"));
					part.setContainer(resultSet.getInt("container"));
					part.setRetailPrice(resultSet.getFloat("retailprice"));
					part.setLineitem(lineitem);
					lineitem.setPart(part);
				}
				lineitems.add(lineitem);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return lineitems;
	}

	@SuppressWarnings("serial")
	public static class OrdersNotFoundException extends Throwable {
		OrdersNotFoundException(String message) {
			super(message);
		}
	}

}
