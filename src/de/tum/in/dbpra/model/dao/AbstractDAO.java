package de.tum.in.dbpra.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import de.tum.in.dbpra.DBSettings;

public abstract class AbstractDAO {
	
	private Connection connection = null;

    protected Connection getConnection() throws SQLException {
    	
    	if (this.connection == null) {
	    	try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				throw new SQLException("PostgreSQL driver is not loaded.");
			};
			
			this.connection = DriverManager.getConnection(
					DBSettings.CONNECTION_STRING, DBSettings.DB_USERNAME, DBSettings.DB_PASSWORD);
    	}
    	
    	return this.connection;
    }
}
