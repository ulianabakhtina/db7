package de.tum.in.dbpra.model.bean;

import java.sql.Date;
import java.util.ArrayList;

public class OrderBean {

	private int orderKey;
	private String orderStatus;
	private float totalPrice;
	private Date orderDate;
	private int custKey;

	private CustomerBean customer;
	private ArrayList<LineitemBean> lineitems;

	public OrderBean() {
	}

	public int getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(int orderKey) {
		this.orderKey = orderKey;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public float getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	
	public int getCustKey() {
		return custKey;
	}
	
	public void setCustKey(int custKey) {
		this.custKey = custKey;
	}

	public CustomerBean getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerBean customer) {
		this.customer = customer;
	}

	public ArrayList<LineitemBean> getLineitems() {
		return lineitems;
	}

	public void setLineitems(ArrayList<LineitemBean> lineitems) {
		this.lineitems = lineitems;
	}

}
