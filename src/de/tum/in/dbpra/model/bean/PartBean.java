package de.tum.in.dbpra.model.bean;

public class PartBean {
	private int partKey;
	private String name;
	private String type;
	private int size;
	private int container;
	private float retailPrice;
	private boolean highlighted;
	private LineitemBean lineitem;
		
	public PartBean() {
	}
	
	public void setHighlighted(boolean highlighted) {
		this.highlighted = highlighted;
	}
	
	public boolean isHighlighted() {
		return highlighted;
	}
	
	public int getPartKey() {
		return partKey;
	}
	public void setPartKey(int partKey) {
		this.partKey = partKey;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getContainer() {
		return container;
	}
	public void setContainer(int container) {
		this.container = container;
	}
	public float getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(float retailPrice) {
		this.retailPrice = retailPrice;
	}
	
	public LineitemBean getLineitem() {
		return lineitem;
	}
	
	public void setLineitem(LineitemBean lineitem) {
		this.lineitem = lineitem;
	}

	public boolean equals(PartBean part) {
		
		if(this.getPartKey()==part.getPartKey()) {
			return true;
		}
		
		else return false;
	}

}
