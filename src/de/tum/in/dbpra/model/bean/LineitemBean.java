package de.tum.in.dbpra.model.bean;

public class LineitemBean {

	private int lineNumber;
	private int quantity;
	private float price;
	private PartBean part;
	private OrderBean order;

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public PartBean getPart() {
		return part;
	}

	public void setPart(PartBean part) {
		this.part = part;
	}

	public OrderBean getOrder() {
		return order;
	}

	public void setOrder(OrderBean order) {
		this.order = order;
	}
}
